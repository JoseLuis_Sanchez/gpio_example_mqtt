#!/usr/bin/env python
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
led = GPIO.setup(14, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)
GPIO.setup(18, GPIO.OUT)

led_r = GPIO.PWM(14, 100)
led_g = GPIO.PWM(15, 100)
led_b = GPIO.PWM(18, 50)

def on_connect(client, userdata,flag, rc):
    print ("Connected with rc: " + str(rc))
    client.subscribe("GPIO")

def on_message(client, userdata, msg):	
    if "red" in msg.payload:
        led_r.start(50)
        time.sleep(5)
        
    if "green" in msg.payload:
        led_g.start(50)
        time.sleep(5)
        
    if "blue" in msg.payload:
        led_b.start(50)
        time.sleep(5)
        
    if "of" in msg.payload:
        led_r.stop()
        led_g.stop()
        led_b.stop()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
try:
    client.connect("test.mosquitto.org",1883)
    print "Conexion Lista"
except:
    print "Fallo Conexion"

client.loop_forever()
