import RPi.GPIO as GPIO
import numpy as np

def led_on(Colores):
    for i in range(len(Colores)):
        #GPIO.output(Colores[i],True)
        Colores[i].start(2)
    return 0

def led_off(Colores):
    for i in range(len(Colores)):
        #GPIO.output(Colores[i],False)
        Colores[i].stop(0)
    return 0

def led_on_red(Colores):
    #GPIO.output(Colores[0],True)
    Colores[0].start(2)
    return 0

def led_off_red(Colores):
    #GPIO.output(Colores[0],False)
    Colores[0].stop()
    return 0

def led_on_green(Colores):
    #GPIO.output(Colores[1],True)
    Colores[1].start(2)
    return 0

def led_off_green(Colores):
    #GPIO.output(Colores[1],False)
    Colores[1].stop()
    return 0

def led_on_blue(Colores):
    #GPIO.output(Colores[2],True)
    Colores[2].start(2)
    return 0

def led_off_blue(Colores):
    #GPIO.output(Colores[2],False)
    Colores[2].stop()
    return 0
