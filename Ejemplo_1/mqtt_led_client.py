#!/usr/bin/env python
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
from clase import led
obj = led()
def on_connect(client, userdata,flag, rc):
    print ("Connected with rc: " + str(rc))
    client.subscribe("GPIO")
    #client.subscribe("led")

def on_message(client, userdata, msg):
    #print ("Topic: "+ msg.topic+"\nMessage: "+str(msg.payload))
    if "red" in msg.payload:
        print("--> Red ON")
        #GPIO.output(14, True)
	led_r.start(5)
    else:
        print("--> Red OFF")
        #GPIO.output(14, False)
	led_r.stop()
    if "green" in msg.payload:
        print("--> Green ON")
        #GPIO.output(15, True)
	led_g.start(5)
    else:
        print("--> Green OFF")
        #GPIO.output(15, False)
	led_g.stop()
    if "blue" in msg.payload:
        print("--> Blue ONN")
        #GPIO.output(18, True)
	led_b.start(5)
    else:
        print("-- Blue OFF")
	print ("")
        #GPIO.output(18, False)
	led_b.stop()
    if "off" in msg.payload:
	print("Apagado")
	led_r.stop()
	led_g.stop()
	led_b.stop()
#Se manda llamar la funcion de otro archivo	
    if "inicia" in msg.payload:
        obj.blink(led_b)

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
try:
    #broker="192.168.0.41"
    #client.connect(broker,1883,60)
    client.connect("test.mosquitto.org",1883)
    print "Conexion Lista"
except:
    print "Fallo Conexion"

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
GPIO.setup(14, GPIO.OUT) ## Color Rojo
GPIO.setup(15, GPIO.OUT) ## Color Verde
GPIO.setup(18, GPIO.OUT) ## Color Azul
#Variables de intencidad
led_r = GPIO.PWM(14, 100)
led_g = GPIO.PWM(15, 100)
led_b = GPIO.PWM(18, 100)
client.loop_forever()
