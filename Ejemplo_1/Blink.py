# -*- coding: utf-8 -*-

import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT) ## GPIO 18 como salida

led_b = GPIO.PWM(18, 100)
def blink():
        print "Ejecución de la función blink..."
        for iteracion in range(10): ## Segundos que durara la funcion                
                led_b.start(5)
                time.sleep(0.5) ## Esperamos 0.5 segundo
                led_b.stop()
                time.sleep(0.5) ## Esperamos 0.5 segundo                
                print(iteracion)
        print "Ejecucion finalizada"
        led_b.stop()
        #GPIO.cleanup() ## Hago una limpieza de los GPIO
# Código principal desde el que usamos todas las funciones        
blink()
