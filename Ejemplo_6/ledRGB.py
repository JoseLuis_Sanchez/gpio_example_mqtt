# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import time
import numpy as np

GPIO.setmode(GPIO.BCM)

GPIO.setup(14, GPIO.OUT) ## Color Rojo
GPIO.setup(15, GPIO.OUT) ## Color Verde
GPIO.setup(18, GPIO.OUT) ## Color Azul
#PWM-INTENCIDAD
led_r = GPIO.PWM(14, 100)
led_g = GPIO.PWM(15, 100)
led_b = GPIO.PWM(18, 100)

Colores = np.array([led_r, led_g, led_b]) # Es importante que el orden sea RGB

def Apagar_led(Leds):
    for i in range(len(Leds)):
        #GPIO.output(Leds[i],False)
        Leds[i].stop()
    return 0

def Encender_leds(Leds):
	for i in range(len(Leds)):
		#GPIO.output(Leds[i],True)
		Leds[i].start(2)
		time.sleep(0.05)
		#GPIO.output(Leds[i],False)
		Leds[i].stop()
	for i in range(len(Leds)):
		#GPIO.output(Leds[len(Leds)-i-1],True)
                Leds[len(Leds)-i-1].start(2)
		time.sleep(0.05)
		#GPIO.output(Leds[len(Leds)-i-1],False)
                Leds[len(Leds)-i-1].stop()
def Encender_mix_rgb(r,g,b, Colores):
    if (r > 0):
        #GPIO.output(Colores[0],True)
        Colores[0].start(2)
    else:
        #GPIO.output(Colores[0],False)
        Colores[0].stop()
    if (g > 0):
        #GPIO.output(Colores[1],True)
        Colores[1].start(2)
    else:
        #GPIO.output(Colores[1],False)
        Colores[1].stop()
    if (b > 0):
        #GPIO.output(Colores[2],True)
        Colores[2].start(2)
    else:
        #GPIO.output(Colores[2],False)
        Colores[2].stop()
    return 0

# Código principal desde el que usamos todas las funciones
Apagar_led(Colores)

for j in range(10):
	Encender_leds(Colores)

# Encendemos el color Cyan
Encender_mix_rgb(0,1,1, Colores)
time.sleep(3)
# Encendemos el color Amarillo
Encender_mix_rgb(1,1,0, Colores)
time.sleep(3)
# Encendemos el color Magenta
Encender_mix_rgb(1,0,1, Colores)
time.sleep(3)
# Encendemos el color Blanco
Encender_mix_rgb(1,1,1, Colores)
time.sleep(3)

print("Limpiando la configuración de los GPIO")
GPIO.cleanup() ## Hago una limpieza de los GPIO
